# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .printer import Printer
from .stock import Location
from .unit_load import UnitLoadTrace, GetPackCode


def register():
    Pool.register(
        Printer,
        Location,
        GetPackCode,
        module='stock_label_printer', type_='model')
    Pool.register(
        UnitLoadTrace,
        module='stock_label_printer', type_='wizard',
        depends=['stock_unit_load_trace'])
