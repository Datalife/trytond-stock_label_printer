# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import doctest
import unittest

import datetime
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction
from trytond.pool import Pool


class TestCase(ModuleTestCase):
    """Test module"""
    module = 'stock_label_printer'

    @with_transaction()
    def test_location_trace(self):
        """Create location"""
        pool = Pool()
        Printer = pool.get('label.printer')
        Location = pool.get('stock.location')
        PrinterType = pool.get('label.printer.type')
        Template = pool.get('label.template')
        TemplateData = Pool().get('label.template.data')

        location1, = Location.create([{
            'name': 'Location 1',
            'type': 'production',
            'trace_code': '01'}])
        self.assert_(location1.id)

        type1, = PrinterType.create([{
                    'name': 'Toshiba EX',
                    'communication_mode': 'bartender'
                    }])

        template1, = Template.create([{
                    'name': 'Template 1',
                    'data': [('create', [{'name': 'Data 1', 'value': 'LOT123'}])]
                    }])
        printer1, = Printer.create([{
                    'name': 'Printer 1',
                    'type_': type1.id,
                    'template': template1.id,
                    'uri': '192.168.1.100@8000'
                    }])

        data1 = TemplateData.search([('name', '=', 'Data 1')])

        TemplateData.write(data1, {
            'value': 'record.location.get_packcode()',
            'engine': 'python'})

        # set location
        Printer.write([printer1], {'location': location1.id})

        # test trace
        _now = datetime.datetime.now().replace(second=0, microsecond=0)
        res = Printer.get_printers_data()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0][0], ('Printer 1', ))
        _trace = res[0][1][0]

        self.assertEqual(_trace[0], 'Data 1')
        self.assertEqual(len(_trace[1]), 6)
        self.assertEqual(_trace[1][2:3], 'T')

        _decoded_trace = Location._decode_trace(_trace[1], datetime.datetime.today().year)
        self.assertEqual(_decoded_trace[0], [location1.id])
        self.assertIsInstance(_decoded_trace[1], datetime.datetime)
        self.assertEqual(_decoded_trace[1], _now)


def suite():
    suite = trytond.tests.test_tryton.suite()
    from trytond.modules.label_printer.tests import test_label_printer
    for test in test_label_printer.suite():
        if test not in suite and not isinstance(test, doctest.DocTestCase):
            suite.addTest(test)
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCase))
    return suite
