# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Printer']


class Printer(metaclass=PoolMeta):
    __name__ = 'label.printer'

    location = fields.Many2One('stock.location', 'Location',
                               domain=[('type', 'in', ['storage', 'production'])],
                               states={'readonly': ~Eval('active')},
                               depends=['active'])
