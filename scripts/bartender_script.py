import xmlrpclib
from datetime import datetime
import sys
import csv
import time

HOST = '<SERVER>'
PORT = '8000'
DB = '<DBNAME>'
USER = '<UID>'
PASSWORD = '<PWD>'

PATH = '<LOCAL_PATH>'


class Tryton(object):

    def __init__(self):
        # Get user_id and session
        self.server = xmlrpclib.ServerProxy('http://%s:%s@%s:%s/%s/' % (
            USER, PASSWORD, HOST, PORT, DB), allow_none=True)

        # Get the user context
        self.pref = self.server.model.res.user.get_preferences(True, {})

    def execute(self, method, *args):
        args += (self.pref,)
        try:
            return getattr(self.server, method)(*args)
        except Exception:
            sys.exit(10)


def generate_csv(_path, data):
    with open('%s/do_print.txt' % _path, 'wb') as _file:
        wr = csv.writer(_file)
        wr.writerow([d[0] for d in data])
        wr.writerow([d[1] for d in data])


if __name__ == '__main__':
    a = Tryton()
    print 'Connected to Tryton!!'
    while True:
        try:
            print '"%s" >> New iteration' % datetime.now()
            printers = a.execute('model.label.printer.get_printers_data',
                ('uri', ), '<TYPE_ID>')
            for printer in printers:
                print '"%s" >> %s' % (printer[0][0], printer[1])
                generate_csv('%s/%s' % (PATH, printer[0][0]), printer[1])
        except Exception as e:
            print 'Service failed getting trace. %s' % str(e)
        time.sleep(60)
