import xmlrpclib
from datetime import datetime
import sys
import time
import socket

HOST = '<SERVER>'
PORT = '8000'
DB = '<DBNAME>'
USER = '<UID>'
PASSWORD = '<PWD>'

BUFFER_SIZE = 1024


class Tryton(object):

    def __init__(self):
        # Get user_id and session
        self.server = xmlrpclib.ServerProxy('http://%s:%s@%s:%s/%s/' % (
            USER, PASSWORD, HOST, PORT, DB), allow_none=True)

        # Get the user context
        self.pref = self.server.model.res.user.get_preferences(True, {})

    def execute(self, method, *args):
        args += (self.pref,)
        try:
            return getattr(self.server, method)(*args)
        except Exception:
            sys.exit(10)


def send_trace(uri, data):
    _ip, _port = uri.split('@')
    _port, _printer = _port.split('//')
    cli_command = 'devices|lookup|%s|update|' % _printer
    for _data_item in data:
        cli_command += '%s=%s' % (_data_item[0], _data_item[1])
    cli_command += '\r\n'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((_ip, int(_port)))
    s.send(cli_command)
    s.close()


if __name__ == '__main__':
    a = Tryton()
    print 'Connected to Tryton!!'
    while True:
        try:
            print '"%s" >> New iteration' % datetime.now()
            printers = a.execute('model.label.printer.get_printers_data',
                ('uri', ), '<TYPE_ID>')
            for printer in printers:
                print '"%s" >> %s' % (printer[0][0], printer[1])
                send_trace(printer[0][0], printer[1])
        except Exception as e:
            print 'Service failed getting trace. %s' % str(e)
        time.sleep(60)
